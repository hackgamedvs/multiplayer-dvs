// IMPORTS
const http = require('http');
const fs = require('fs');
// const cluster = require('cluster');
// const numCPUs = require('os').cpus().length;
const io = require('socket.io');
const p2 = require('p2');
// CONSTRAINS
const PORT = 3000;
const rooms = [];
const MAX_PLAYER = 200;
const MAX_ROOM = 20;
const NORMAL_SPEED = 40;
const NORMAL_ANGLE_SPEED = 100;
const DELTA_INTERVAL = 1000 / 60;
const DELTA = DELTA_INTERVAL / 1000;
const tagNmask = {
    WALL: 1,
    PLAYER: 2
}
//INIT WORLD EACH ROOM
for (let i = 0; i < MAX_ROOM; i++) {
    const world = new p2.World({
        gravity: [0, 0]
    });
    const groundBody = new p2.Body();
    groundBody.addShape(new p2.Box({
        width: 600,
        height: 10,
    }));
    groundBody.shapes[0].collisionMask = tagNmask.WALL;
    groundBody.shapes[0].collisionGroup = tagNmask.PLAYER;
    groundBody.userData = {
        tag: tagNmask.WALL
    }
    groundBody.position = [0, 300];
    world.addBody(groundBody);
    const groundBody1 = new p2.Body();
    groundBody1.addShape(new p2.Box({
        width: 600,
        height: 10
    }));
    groundBody1.shapes[0].collisionMask = tagNmask.WALL;
    groundBody1.shapes[0].collisionGroup = tagNmask.PLAYER;
    groundBody1.userData = {
        tag: tagNmask.WALL
    }
    groundBody1.position = [0, -300];
    world.addBody(groundBody1);
    const groundBody2 = new p2.Body();
    groundBody2.addShape(new p2.Box({
        width: 10,
        height: 600
    }));
    groundBody2.shapes[0].collisionMask = tagNmask.WALL;
    groundBody2.shapes[0].collisionGroup = tagNmask.PLAYER;
    groundBody2.userData = {
        tag: tagNmask.WALL
    }
    groundBody2.position = [-300, 0];
    world.addBody(groundBody2);
    const groundBody3 = new p2.Body();
    groundBody3.addShape(new p2.Box({
        width: 10,
        height: 600
    }));
    groundBody3.shapes[0].collisionMask = tagNmask.WALL;
    groundBody3.shapes[0].collisionGroup = tagNmask.PLAYER;
    groundBody3.userData = {
        tag: tagNmask.WALL
    }
    groundBody3.position = [300, 0];
    world.addBody(groundBody3);
    rooms.push({
        id: i,
        world,
        count: 0,
        sockets: []
    });
    world.on('beginContact', collisions)
}
// PROCESS SERVER
let ioServer = io();

const server = http.createServer((req, res) => {
    fs.readFile(__dirname + '/index.html',
        function (err, data) {
            if (err) {
                res.writeHead(500);
                return res.end('Error loading index.html');
            }
            res.writeHead(200);
            res.end(data);
        });
}).listen(process.env.PORT || PORT);

socketHandler(server);
// HANDLE SOCKET SERVER
function socketHandler(sv) {
    ioServer = io(sv);
    ioServer.on('connection', socket => {
        let name;
        socket.leaveAll();
        const room = rooms.find(r => r.count < MAX_PLAYER);
        if (room) {
            const me = new p2.Body({mass: 1});
            me.addShape(new p2.Circle({
                radius: 3,
                collisionMask: tagNmask.PLAYER,
                collisionGroup: tagNmask.WALL
            }));
            me.angle = 90;
            me.userData = {
                name: name || me.id,
                tag: tagNmask.PLAYER,
                socket,
                offset: [0, 1],
                angle: 90
            };
            room.count += 1;
            socket.on('..', (angle) => {
                me.userData.angle = angle < 0 ? Math.PI * 2 + angle : angle;
            });
            socket.once('name', (n) => {
                me.userData.name = n;
                room.world.addBody(me);
                socket.join(room.id);
            });
            socket.on('disconnect', () => {
                room.count -= 1;
                room.world.removeBody(me);
                socket.leave(room.id);
            });
        } else {
            // LIMIT SERVER
        }
    });
}

setInterval(() => {
    rooms.forEach(room => {
        room.world.step(DELTA);
        room.world.bodies
            .filter(b => b.userData.tag === tagNmask.PLAYER)
            .forEach(b => {
                let angle = b.userData.angle;
                if (angle - b.angle / 180 < Math.PI || b.angle - angle / 180 < -Math.PI) {
                    b.angle += NORMAL_ANGLE_SPEED * DELTA;
                }
                else {
                    b.angle -= NORMAL_ANGLE_SPEED * DELTA;
                }
                angle = b.angle / 180;
                if (angle >= 0 && angle < Math.PI / 2) {
                    b.userData.offset[0] = Math.cos(angle);
                    b.userData.offset[1] = Math.sin(angle);
                } else if (angle >= Math.PI / 2 && angle <= Math.PI) {
                    b.userData.offset[0] = -Math.sin(angle - Math.PI / 2);
                    b.userData.offset[1] = Math.cos(angle - Math.PI / 2);
                } else if (angle < 2 * Math.PI && angle > 3 * Math.PI / 2) {
                    b.userData.offset[0] = Math.cos(2 * Math.PI - angle);
                    b.userData.offset[1] = -Math.sin(2 * Math.PI - angle);
                } else {
                    b.userData.offset[0] = -Math.cos(angle - Math.PI);
                    b.userData.offset[1] = -Math.sin(angle - Math.PI);
                }
                b.position[0] += NORMAL_SPEED * b.userData.offset[0] * DELTA;
                b.position[1] += NORMAL_SPEED * b.userData.offset[1] * DELTA;
            })
        ioServer.to(room.id).emit('.', room.world.bodies.map(b => [parseFloat(b.position[0].toFixed(2)), parseFloat(b.position[1].toFixed(2)), parseFloat(b.angle.toFixed(2)), b.id, b.userData.name]).slice(4));
    })
}, DELTA_INTERVAL);

function collisions(e) {
    if (e.bodyA.userData.tag === tagNmask.PLAYER || e.bodyB.userData.tag === tagNmask.PLAYER) {
        const player = e.bodyA.userData.tag === tagNmask.PLAYER ? e.bodyA : e.bodyB;
        const target = e.bodyA.userData.tag === tagNmask.PLAYER ? e.bodyB : e.bodyA;
        player.userData.socket.emit('die');
        player.world.removeBody(player);
    }
};